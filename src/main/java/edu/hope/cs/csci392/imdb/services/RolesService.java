package edu.hope.cs.csci392.imdb.services;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import edu.hope.cs.csci392.imdb.Database;
import edu.hope.cs.csci392.imdb.model.Role;

public class RolesService extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String type = (String) request.getAttribute("type");
		int id = Integer.parseInt((String) request.getAttribute("id"));
		
		List<Role> roles = null;
		Database db = Database.getInstance();
		Gson gson = new Gson ();
		
		if (type.equals ("Actor")) {
			roles = db.findMoviesForActor(db.findActorByID(id));
		}
		else {
			roles = db.findRolesInMovie(db.findMovieByID(id));
		}
		
		for (Role role : roles) {
			role.getMovie();
			role.getActor();
		}
		
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().println(gson.toJson(roles));
	}
}
